package fr.asi.scrum.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="etudiant_offre")
@IdClass(EtudiantOffreId.class)
public class Etudiant_offre implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
    @ManyToOne
    @JoinColumn(name = "etudiant_id", referencedColumnName = "id")
    private Etudiant etudiant;

	@Id
    @ManyToOne
    @JoinColumn(name = "offre_id", referencedColumnName = "id")
    private Offre offre;

	@Enumerated(EnumType.STRING)
    @Column(name = "suivi")
    private Suivi suivi;

	public Etudiant_offre() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Etudiant getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}

	public Offre getOffre() {
		return offre;
	}

	public void setOffre(Offre offre) {
		this.offre = offre;
	}

	public Suivi getSuivi() {
		return suivi;
	}

	public void setSuivi(Suivi suivi) {
		this.suivi = suivi;
	}	
}
