package fr.asi.scrum.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;


@Entity

public class Etudiant implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="nom", nullable=true)
	private String nom;
	
	@Column(name="prenom", nullable=true)
	private String prenom;
	
	@Column(name="cv", length = 16777215, nullable=true)
	private byte[] cv;
	
	@Column(name="lettre_de_motivation", length = 16777215, nullable=true)
	private byte[] lettre_de_motiviation;
	
	@Column(name="permisB")
	private Boolean permisB;
	
	@Column(name="secteur")
	private String secteur;
	
	@Column(name="ville", nullable=true)
	private String ville;
	
	@Column(name="adresse", nullable=true)
	private String adresse;
	
	@Column(name="codePostal", nullable=true)
	private String codePostal;
	
	@Column(name="telephone", nullable=true)
	private String telephone;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id",referencedColumnName="id" , nullable=true)
	private User user;
	
	@ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(name="diplome_etudiant", 
    			joinColumns=@JoinColumn(name="id_etudiant", referencedColumnName="id", nullable=false),
    			inverseJoinColumns = @JoinColumn(name="id_diplome", referencedColumnName="id", nullable=false))
	private List<Diplome> diplomes;
	
	public Etudiant() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Etudiant(int id, String nom, String prenom, byte[] cv, byte[] lettre_de_motiviation, Boolean permisB,
			String secteur, String ville, String adresse, String codePostal, String telephone, User user, List<Diplome> diplomes) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.cv = cv;
		this.lettre_de_motiviation = lettre_de_motiviation;
		this.permisB = permisB;
		this.secteur = secteur;
		this.ville = ville;
		this.adresse = adresse;
		this.codePostal = codePostal;
		this.telephone = telephone;
		this.user = user;
		this.diplomes = diplomes;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public byte[] getCv() {
		return cv;
	}

	public void setCv(byte[] cv) {
		this.cv = cv;
	}

	public byte[] getLettre_de_motiviation() {
		return lettre_de_motiviation;
	}

	public void setLettre_de_motiviation(byte[] lettre_de_motiviation) {
		this.lettre_de_motiviation = lettre_de_motiviation;
	}

	public Boolean getPermisB() {
		return permisB;
	}

	public void setPermisB(Boolean permisB) {
		this.permisB = permisB;
	}

	public String getSecteur() {
		return secteur;
	}

	public void setSecteur(String secteur) {
		this.secteur = secteur;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Diplome> getDiplomes() {
		return diplomes;
	}

	public void setDiplomes(List<Diplome> diplomes) {
		this.diplomes = diplomes;
	}
	
}
