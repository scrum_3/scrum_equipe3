package fr.asi.scrum.dao;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

import fr.asi.scrum.entities.Etudiant;


public class EtudiantDAO {
	
	private static final String JPA_UNIT_NAME = "scrum";
    private static EntityManager entityManager;

    protected static EntityManager getEntityManager() {
        if (entityManager == null) {
            entityManager = Persistence.createEntityManagerFactory(
                    JPA_UNIT_NAME).createEntityManager();
        }
        return entityManager;
    }
    
    public List<Etudiant> selectAll() {
        List<Etudiant> etudiant = getEntityManager().createQuery("select e from Etudiant e").getResultList();
        return etudiant;
    }
    
    public static Etudiant insert(Etudiant etudiant) {
        getEntityManager().getTransaction().begin();
        getEntityManager().persist(etudiant);
        getEntityManager().getTransaction().commit();
        return etudiant;
    }

    public void delete(Etudiant u) {
    	System.out.println("je suis passer ici frerot");
        getEntityManager().getTransaction().begin();
        u = getEntityManager().merge(u);//<-Important
        getEntityManager().remove(u);
        getEntityManager().getTransaction().commit();
    }
    
    public Etudiant update(Etudiant u) {
        getEntityManager().getTransaction().begin();
        u = getEntityManager().merge(u);
        getEntityManager().getTransaction().commit();
        return u;
    }

}
