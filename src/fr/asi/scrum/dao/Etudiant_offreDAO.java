package fr.asi.scrum.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

import fr.asi.scrum.entities.Etudiant_offre;

public class Etudiant_offreDAO {
	private static final String JPA_UNIT_NAME = "scrum";
    private static EntityManager entityManager;

    protected static EntityManager getEntityManager() {
        if (entityManager == null) {
            entityManager = Persistence.createEntityManagerFactory(
                    JPA_UNIT_NAME).createEntityManager();
        }
        return entityManager;
    }
    
    public List<Etudiant_offre> selectAll() {
        List<Etudiant_offre> etudiant_offre = getEntityManager().createQuery("FROM Etudiant_offre").getResultList();
        return etudiant_offre;
    }
    
    public static Etudiant_offre insert(Etudiant_offre etudiant_offre) {	
        getEntityManager().getTransaction().begin();
        getEntityManager().persist(etudiant_offre);
        getEntityManager().getTransaction().commit();
        return etudiant_offre;
    }

    public void delete(Etudiant_offre u) {
        getEntityManager().getTransaction().begin();
        u = getEntityManager().merge(u);//<-Important
        getEntityManager().remove(u);
        getEntityManager().getTransaction().commit();
    }
    
    public Etudiant_offre update(Etudiant_offre u) {
        getEntityManager().getTransaction().begin();
        u = getEntityManager().merge(u);
        getEntityManager().getTransaction().commit();
        return u;
    }

}
