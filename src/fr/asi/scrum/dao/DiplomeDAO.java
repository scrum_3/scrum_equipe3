package fr.asi.scrum.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import fr.asi.scrum.entities.Diplome;

public class DiplomeDAO {
	private static final String JPA_UNIT_NAME = "scrum";
    private static EntityManager entityManager;

    protected static EntityManager getEntityManager() {
        if (entityManager == null) {
            entityManager = Persistence.createEntityManagerFactory(
                    JPA_UNIT_NAME).createEntityManager();
        }
        return entityManager;
    }
    
    public List<Diplome> selectAll() {
        List<Diplome> diplome = getEntityManager().createQuery("FROM Diplome").getResultList();
        return diplome;
    }
    
    public static Diplome insert(Diplome d) {	
        getEntityManager().getTransaction().begin();
        getEntityManager().persist(d);
        getEntityManager().getTransaction().commit();
        return d;
    }

    public void delete(Diplome u) {
        getEntityManager().getTransaction().begin();
        u = getEntityManager().merge(u);//<-Important
        getEntityManager().remove(u);
        getEntityManager().getTransaction().commit();
    }
    
    public Diplome update(Diplome u) {
        getEntityManager().getTransaction().begin();
        u = getEntityManager().merge(u);
        getEntityManager().getTransaction().commit();
        return u;
    }
}
