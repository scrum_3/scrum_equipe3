package fr.asi.scrum.managedBeans;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.asi.scrum.dao.DiplomeDAO;
import fr.asi.scrum.dao.EtudiantDAO;
import fr.asi.scrum.entities.Diplome;
import fr.asi.scrum.entities.Etudiant;

@ManagedBean(name = "DiplomeBean")
@SessionScoped
public class DiplomeBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private DiplomeDAO dDao = new DiplomeDAO();
	private Diplome diplome = new Diplome();
	private Diplome selectedDiplome;
	private Diplome editDiplome;

	public List<Diplome> getDiplomes() {
		return dDao.selectAll();
	}
	public String addiplome(Etudiant e){
		
		return "list_diplome";
	}

	public String createDiplome(Diplome e) {
		dDao.insert(e);
		return "RIEN";
	}

	public String deleteDiplome(Diplome e) {
		dDao.delete(e);
		return null;
	}

	public String showDiplome(Diplome e) {
		selectedDiplome = e;
		return "id";
	}

	public String edit(Diplome e) {
		editDiplome = e;
		return "value";
	}

	public String updateDiplomet() {
		dDao.update(editDiplome);
		return "list";
	}

	public DiplomeDAO getdDao() {
		return dDao;
	}

	public void setdDao(DiplomeDAO dDao) {
		this.dDao = dDao;
	}

	public Diplome getDiplome() {
		return diplome;
	}

	public void setDiplome(Diplome diplome) {
		this.diplome = diplome;
	}

	public Diplome getSelectedDiplome() {
		return selectedDiplome;
	}

	public void setSelectedDiplome(Diplome selectedDiplome) {
		this.selectedDiplome = selectedDiplome;
	}

	public Diplome getEditDiplome() {
		return editDiplome;
	}

	public void setEditDiplome(Diplome editDiplome) {
		this.editDiplome = editDiplome;
	}

	public DiplomeBean(DiplomeDAO dDao, Diplome diplome, Diplome selectedDiplome, Diplome editDiplome) {
		super();
		this.dDao = dDao;
		this.diplome = diplome;
		this.selectedDiplome = selectedDiplome;
		this.editDiplome = editDiplome;
	}
	public DiplomeBean(){
		
	}

}
