package fr.asi.scrum.managedBeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.primefaces.event.FlowEvent;

import com.mysql.cj.conf.PropertyDefinitions.ZeroDatetimeBehavior;

import fr.asi.scrum.dao.EtudiantDAO;
import fr.asi.scrum.entities.Diplome;
import fr.asi.scrum.entities.Etudiant;
import fr.asi.scrum.entities.FileUpload;
import fr.asi.scrum.jpa.listeners.ApplicationListener;

@ManagedBean(name = "EtudiantBean")
@SessionScoped
public class EtudiantBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private UploadedFile uploadedCV;
	private UploadedFile uploadedLM;
	
	private EtudiantDAO eDao = new EtudiantDAO();
	private Etudiant etudiant = new Etudiant();
	private Etudiant etudiantsel;
	private Etudiant editEtudiant;
	
	public void showPDF(byte[] pdf) throws IOException {
		FacesContext faces = FacesContext.getCurrentInstance();
		HttpServletResponse response = (HttpServletResponse) faces.getExternalContext().getResponse();
		response.setContentType("application/pdf");
		response.setContentLength(pdf.length);
		//response.setHeader("Content-disposition", "inline; filename=\"" + fu.getTitre() + "\"");
		try {
			ServletOutputStream out;
			out = response.getOutputStream();
			out.write(pdf);
		} catch (IOException e) {
			e.printStackTrace();
		}
		faces.responseComplete();
	}
	private boolean skip;

	public String add(){
		return "page_formEtudiant";
	}

	public String test(Etudiant e) {
		if (e.getPermisB() != null) {
			if (e.getPermisB() == true) {
				return "Oui";
			} else if (e.getPermisB() == false) {
				return "Non";
			}
		} else {
			return "Non renseigné";
		}
		return "PermisB";
	}

	public List<Etudiant> getEtudiants() {

		return eDao.selectAll();
	}

	public String createEtudiant(Etudiant e){
		try {
			getEtudiant().setCv(getUploadedCV().getBytes());
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		try {
			getEtudiant().setLettre_de_motiviation(getUploadedLM().getBytes());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		eDao.insert(e);
		return "RIEN";
	}

	public Void deleteEtudiant(Etudiant e) {
		eDao.delete(e);
		return null;
	}

	public String showEtudiant(Etudiant e) {
		etudiantsel = e;
		return "id";
	}

	public String edit(Etudiant e) {
		editEtudiant = e;
		return "value";
	}

	public String updateEtudiant() {
		eDao.update(editEtudiant);
		return "list";
	}

	public List<Diplome> getdiplome(Etudiant e) {
		return e.getDiplomes();
	}

	public EtudiantDAO geteDao() {
		return eDao;
	}

	public void seteDao(EtudiantDAO eDao) {
		this.eDao = eDao;
	}

	public Etudiant getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}

	public Etudiant getEtudiantsel() {
		return etudiantsel;
	}

	public void setEtudiantsel(Etudiant etudiantsel) {
		this.etudiantsel = etudiantsel;
	}

	public Etudiant getEditEtudiant() {
		return editEtudiant;
	}

	public void setEditEtudiant(Etudiant editEtudiant) {
		this.editEtudiant = editEtudiant;
	}
	
	public UploadedFile getUploadedCV() {
		return uploadedCV;
	}

	public void setUploadedCV(UploadedFile uploadedCV) {
		this.uploadedCV = uploadedCV;
	}

	public UploadedFile getUploadedLM() {
		return uploadedLM;
	}

	public void setUploadedLM(UploadedFile uploadedLM) {
		this.uploadedLM = uploadedLM;
	}

	public EtudiantBean(EtudiantDAO eDao, Etudiant etudiant, Etudiant etudiantsel, Etudiant editEtudiant) {
		super();
		this.eDao = eDao;
		this.etudiant = etudiant;
		this.etudiantsel = etudiantsel;
		this.editEtudiant = editEtudiant;
	}

	public EtudiantBean() {
	}
	
	public String onFlowProcess(FlowEvent event) {
        if(skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        }
        else {
            return event.getNewStep();
        }
    }

}
