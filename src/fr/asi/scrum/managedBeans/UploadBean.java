package fr.asi.scrum.managedBeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.myfaces.custom.fileupload.UploadedFile;

import fr.asi.scrum.entities.FileUpload;
import fr.asi.scrum.jpa.listeners.ApplicationListener;

@ManagedBean(name = "uploadBean")
@RequestScoped
public class UploadBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private UploadedFile uploadedFile;
	private List<FileUpload> fus;

	public void download(FileUpload fu) throws IOException {
		byte[] pdf = fu.getPdf();

		FacesContext faces = FacesContext.getCurrentInstance();
		HttpServletResponse response = (HttpServletResponse) faces.getExternalContext().getResponse();
		response.setContentType("application/pdf");
		response.setContentLength(pdf.length);
		response.setHeader("Content-disposition", "inline; filename=\"" + fu.getTitre() + "\"");
		try {
			ServletOutputStream out;
			out = response.getOutputStream();
			out.write(pdf);
		} catch (IOException e) {
			e.printStackTrace();
		}
		faces.responseComplete();
	}

	public void submit() throws IOException {
		String fileName = FilenameUtils.getName(uploadedFile.getName());
		String contentType = uploadedFile.getContentType();
		byte[] bytes = uploadedFile.getBytes();
		// Now you can save bytes in DB (and also content type?)
		FileUpload fu = new FileUpload();
		fu.setTitre(fileName);
		fu.setPdf(bytes);

		EntityManager em = ApplicationListener.getEmf().createEntityManager();
		em.getTransaction().begin();
		em.persist(fu);
		em.getTransaction().commit();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
				String.format("File '%s' of type '%s' successfully uploaded!", fileName,contentType)));
	}

	@PostConstruct
	public void init() {
		EntityManager em = ApplicationListener.getEmf().createEntityManager();
		em.getTransaction().begin();
		Query query = em.createQuery("SELECT fu FROM FileUpload fu");
		fus = query.getResultList();
		System.out.print(fus);
		em.getTransaction().commit();
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public List<FileUpload> getFus() {
		return fus;
	}

	public void setFus(List<FileUpload> fus) {
		this.fus = fus;
	}
}
