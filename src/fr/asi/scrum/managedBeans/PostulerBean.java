package fr.asi.scrum.managedBeans;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.asi.scrum.dao.Etudiant_offreDAO;
import fr.asi.scrum.entities.Etudiant;
import fr.asi.scrum.entities.Etudiant_offre;

@ManagedBean(name = "PostulerBean")
@SessionScoped
public class PostulerBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Etudiant_offreDAO eDao = new Etudiant_offreDAO();
	private Etudiant_offre offre = new Etudiant_offre();
	private Etudiant_offre selectedOffre;
	private Etudiant selectedEtudiant;
	private Etudiant_offre editOffre;
	private String suivioffre;

	public List<Etudiant_offre> getOffre() {
		return eDao.selectAll();
	}

	public String createOffre(Etudiant_offre e) {
		eDao.insert(e);
		return "list";
	}

	public String deleteOffre(Etudiant_offre e) {
		eDao.delete(e);
		return null;
	}

	public String showOffre(Etudiant e) {

		for (int i = 0; i < eDao.selectAll().size(); i++) {

			selectedOffre = eDao.selectAll().get(i);
			System.out.println(e.getId() + "    " + selectedOffre);
			if (selectedOffre.getEtudiant().getId() == e.getId()) {
				switch (selectedOffre.getSuivi().toString()) {
				case "Valider":
					suivioffre = "3";
					break;
				case "Transmis":
					suivioffre = "1";
					break;
				case "En_cours":
					suivioffre = "2";
					break;
				}
				return "suivi_valide";
			}
		}
		return "non";
	}

	public String edit(Etudiant_offre e) {
		editOffre = e;
		return "value";
	}

	public String updateOffre() {
		eDao.update(editOffre);
		return "list";
	}

	public Etudiant_offreDAO geteDao() {
		return eDao;
	}

	public void seteDao(Etudiant_offreDAO eDao) {
		this.eDao = eDao;
	}

	public Etudiant_offre getSelectedOffre() {
		return selectedOffre;
	}

	public void setSelectedOffre(Etudiant_offre selectedOffre) {
		this.selectedOffre = selectedOffre;
	}

	public Etudiant_offre getEditOffre() {
		return editOffre;
	}

	public void setEditOffre(Etudiant_offre editOffre) {
		this.editOffre = editOffre;
	}

	public void setOffre(Etudiant_offre offre) {
		this.offre = offre;
	}

	public Etudiant getSelectedEtudiant() {
		return selectedEtudiant;
	}

	public void setSelectedEtudiant(Etudiant selectedEtudiant) {
		this.selectedEtudiant = selectedEtudiant;
	}

	public String getSuivioffre() {
		return suivioffre;
	}

	public void setSuivioffre(String suivioffre) {
		this.suivioffre = suivioffre;
	}

	public PostulerBean(Etudiant_offreDAO eDao, Etudiant_offre offre, Etudiant_offre selectedOffre,
			Etudiant_offre editOffre) {
		super();
		this.eDao = eDao;
		this.offre = offre;
		this.selectedOffre = selectedOffre;
		this.editOffre = editOffre;
	}

	public PostulerBean() {

	}

}
